# Portfolio

Welcome to my Portfolio!

My name is Jeremy Bowen and I am a recent college grad with development experience at two successful companies. This portfolio contains some different files from school and personal projects.

The web folder contains the portfolio website with basic, static website while the projects folder contains zip files of some different projects I've worked on in various languages.

If you haven't been to the website, the link is [portfolio.jbwnn.com](https://portfolio.jbwnn.com) which also has some basic contact information.

