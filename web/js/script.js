$(document).ready(function(){
	function multipleBoxShadow(n, height){
		var value = Math.floor(Math.random() * height)+"px "+Math.floor(Math.random() * height)+"px #FFF";
		for(var i = 2; i < n; i++){
			value = value + ", " + Math.floor(Math.random() * height)+"px "+Math.floor(Math.random() * height)+"px #FFF";
		}
		return value;
	}
	let height= $("#content").height();
	if(height > 1100){
		$('.stars').css({ boxShadow: multipleBoxShadow(700, height)});
		$('.stars2').css({ boxShadow: multipleBoxShadow(500, height)});
		$('.stars3').css({ boxShadow: multipleBoxShadow(200, height)});
	} else{
		$('.stars').css({ boxShadow: multipleBoxShadow(700, height)});
		$('.stars2').css({ boxShadow: multipleBoxShadow(500, height)});
		$('.stars3').css({ boxShadow: multipleBoxShadow(200, height)});
	}
});
