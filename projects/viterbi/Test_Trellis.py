from trellis import *

avalanche_states = ('High', 'Low', 'End')
avalanche_evidence_variables = tuple('Note')

avalanche_transition_probs = {
    'High': {'High': 0.6, 'Low': 0.4, 'End':0},
    'Low':  {'High': 0.4, 'Low': 0.5, 'End':0.1},
    'End':  {'High': 0,   'Low': 0,   'End':1}
}

avalanche_emission_probs = {
    'High' : {'Note':[0.1, 0.1, 0.4, 0.4]},
    'Low'  : {'Note':[0.4, 0.4, 0.1, 0.1]},
    'End'  : {'Note':[1e-4, 1e-4, 1e-4, 1e-4]}
}

prior = {'High':0.5, 'Low':0.5}
invasion_prior = {'High':0.33, 'Mid':0.33, 'Low':0.33}
invasion_states = ('High', 'Mid', 'Low', 'End')
invasion_evidence_variables = ('Note')


invasion_transition_probs = {
    'High': {'High': 0.6, 'Mid':0.4 , 'Low': 0, 'End':0},
    'Mid' : {'High': 0,  'Mid':0.7, 'Low': 0.3, 'End': 0 },
    'Low':  {'High': 0,   'Mid': 0,    'Low': 0.6, 'End':0.4},
    'End':  {'High': 0,   'Mid': 0,    'Low': 0,   'End':1}
}

invasion_emission_probs = {
    'High' : {'Note':[0.1, 0.1, 0.2, 0.6]},
    'Mid'  : {'Note':[0.1, 0.1, 0.7, 0.1]},
    'Low'  : {'Note':[0.6, 0.2, 0.1, 0.1]},
    'End'  : {'Note':[1e-4, 1e-4, 1e-4, 1e-4]}
}
# D# D C# C C# D D# D C# C
evidence_sequence_1 = [{'Note':3}, {'Note':2}, {'Note':1},{'Note':0},{'Note':1},{'Note':2},{'Note':3},{'Note':2},{'Note':1},{'Note':0}]

invasion_state_sequence_1 = viterbi(evidence_sequence_1, invasion_prior, invasion_states, invasion_evidence_variables, invasion_transition_probs, invasion_emission_probs)
avalanche_state_sequence_1 = viterbi(evidence_sequence_1, prior, avalanche_states,avalanche_evidence_variables, avalanche_transition_probs, avalanche_emission_probs )


if invasion_state_sequence_1 == ['High', 'Mid', 'Mid', 'Mid', 'Mid', 'Mid', 'Mid', 'Mid', 'Low', 'Low']:
    print("Invasion Sequence Correct")
else:
    print("Invasion Sequence Incorrect")

if avalanche_state_sequence_1 == ['High', 'High', 'Low', 'Low', 'Low', 'High', 'High', 'High', 'Low', 'Low']:
    print("Avalanche Sequence Correct")
else:
    print("Avalanche Sequence Incorrect")



