/**
 *A Word Node for creating Word based Link Lists
 *
 *@author Jeremy Bowen
 *@version 10/12/17
 */

public class WordNode{
   private Word w;
   private WordNode node;
   
   
   public Word getWord(){return w;}
   
   public WordNode getNext(){return node;}
   
   public void changeWord(Word newWord){w = newWord;}
   
   public void changeNext(WordNode newNode){node = newNode;}
   
   public String toString(){return w+"";}
}