/**
 * A hash table for storing Word objects using chaining
 *
 * @author Jeremy Bowen
 * @version 10/14/17
 */

public class TranslateHash{
   private WordList[] T;
   private int TABLESIZE, n, longest, listcount;
   
   public TranslateHash(){
      TABLESIZE = 400000;
      T = new WordList[TABLESIZE];
      n = 0;
      longest = 0;
      listcount = 0;
   }
   
   /**
    * Inserts a new Word into the hash table
    *
    * @param w the Word to be inserted
    */
   public void insert(Word w){
      int h = hash(w);
      if(T[h]==null){
         T[h] = new WordList();
         listcount++;
      }
      T[h].insert(w);
      if(T[h].getSize()>longest){
         longest = T[h].getSize();
      }
      n++;
   }
   
   /**
    * Searches the hash table for the given Word starting at a given point
    * 
    * @param w the object of the search
    * @param n the starting position of the search
    * @returns the index of the object in the Link List
    */
    
   public int search(Word w, int n){
      int h = hash(w);
      if(T[h]!=null){
         return T[h].search(w, n);
      }
      return -1;
   }
   
   
   /**
    * Gets a specific WordNode from the hash table
    *
    * @param w the Word one is trying to get
    * @param n the index of the Word in the Link List
    * @return the WordNode with Word w and index n
    */
    
   public WordNode getWord(Word w, int n){
      int h = hash(w);
      return T[h].getWord(n);
   }
   
   /**
    * Gets the Link List for Word w
    *
    * @param w the Word of the Link List
    * @return the WordList containing all the Words with the same hash code
    */
    
   public WordList getList(Word w){
      int h = hash(w);
      
      return T[h];
   }
   
   public int getSize(){return n;}
   
   public int getLongest(){return longest;}
   
   public int getListCount(){return listcount;}
   
   public int getTablesize(){return TABLESIZE;}
   
   public int hash(Word w){
      return hashB(w);
   }
   
   /**
    * The hash code written in class
    *
    * @param w the Word one is trying to get the hash code of
    * @return all the ACSII values of each character in the word multiplied together
    */
    
   public int hashA(Word w){
      int val = 1;
      String str = w.trnsl;
      for(int i = 0; i<str.length(); i++){
         val *=(int) str.charAt(i);
      } 
      return ((Math.abs(val))%TABLESIZE);
   }
   
   /**
    * Java's default hash code
    * 
    * @param w the Word one is trying to get the hash code of
    * @return Java's default value
    */
    
   public int hashB(Word w){
      return Math.abs(w.trnsl.hashCode())%TABLESIZE;
   }
   
   /**
    * A custom hash code
    *
    * @param w the Word one is trying to get the hash code of
    * @return all the ASCII values multiplied by each other, the final character of the Word times, and the length of the Word
    */
    
   public int hashC(Word w){
      int val = 1;
      String str = w.trnsl;
      
      for(int i=0;i<str.length();i++){
         str.charAt(i);
         val *= str.charAt(i);
      }
      val *= str.charAt(str.length()-1)*(str.length());
      return Math.abs(val%TABLESIZE);
   }
}