/**
 *A String Node for creating String based Link Lists
 *
 *@author Jeremy Bowen
 *@version 9/5/17
 */

public class StringNode{
   private String s;
   private StringNode node;
   
   
   public String getString(){return s;}
   
   public StringNode getNext(){return node;}
   
   public void changeString(String newString){s = newString;}
   
   public void changeNext(StringNode newNode){node = newNode;}
   
   public String toString(){return s;}
}