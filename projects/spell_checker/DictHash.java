/**
 * A hash table for storing Strins using chaining
 *
 * @author Jeremy Bowen
 * @version 10/14/17
 */

public class DictHash{
   private StringList[] T;
   private int TABLESIZE, n, longest, listCount;
   
   public DictHash(){
      TABLESIZE = 400000;
      T = new StringList[TABLESIZE];
      n = 0;
      longest = 0;
      listCount = 0;
   }
   
   /**
    * Inserts a new String into the hash table
    *
    * @param s the String to be inserted
    */
    
   public void insert(String s){
      int h = hash(s);
      if(T[h]==null){
         T[h] = new StringList();
         listCount++;
      }
      T[h].insert(s);
      if(T[h].getSize()>longest){
         longest = T[h].getSize();
      }
      n++;
   }
   
   /**
    * Searches the hash table for the given String starting at a given point
    * 
    * @param w the object of the search
    * @param n the starting position of the search
    * @returns the index of the object in the Link List
    */

   public int search(String s, int n){
      int h = hash(s);
      
      if(T[h]!=null){
         return T[h].search(s, n);
      }
      return -1;
   }
   
   /**
    * Gets a specific SttringNode from the hash table
    *
    * @param s the String one is trying to get
    * @param n the index of the String in the Link List
    * @return the StringNode with String w and index n
    */
    

   public StringNode getString(String s, int n){
      int h = hash(s);
      return T[h].getString(n);
   }
   
   /**
    * Gets the Link List for String s
    *
    * @param s the String of the Link List
    * @return the StringList containing all the String with the same hash code
    */

   public StringList getList(String s){
      int h = hash(s);
      return T[h];
   }
   
   public int getSize(){return n;}
   
   public int getLongest(){return longest;}
   
   public int getListCount(){return listCount;}
   
   public int getTablesize(){return TABLESIZE;}
   
   public int hash(String s){
      return hashB(s);
   }
   
    /**
    * The hash code written in class
    *
    * @param s the String one is trying to get the hash code of
    * @return all the ACSII values of each character in the word multiplied together
    */
    
   public int hashA(String s){
      int val = 1;
      for(int i = 0; i<s.length(); i++){
         val *=(int) s.charAt(i);
      } 
      return ((Math.abs(val))%TABLESIZE);
   }
   
    /**
    * Java's default hash code
    * 
    * @param s the String one is trying to get the hash code of
    * @return Java's default value
    */
    
   public int hashB(String s){
      return Math.abs(s.hashCode())%TABLESIZE;
   }
   
    /**
    * A custom hash code
    *
    * @param str the String one is trying to get the hash code of
    * @return all the ASCII values multiplied by themselves, by the final character of the String, and by the length of the String

    */
    
   public int hashC (String str){
      int val = 1;
      
      for(int i=0;i<str.length();i++){
         val *= str.charAt(i);         
      }
      val *= str.charAt(str.length()-1);
      return Math.abs(val%TABLESIZE);
   }
}