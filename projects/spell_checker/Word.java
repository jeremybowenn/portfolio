/**
 * An Object that stores both the original word and the phonetic representaion
 *
 * @author Jeremy Bowen
 * @version 10/13/17
 */

public class Word{
   String dict;
   String trnsl;
   
   public Word(){
      dict = null;
      trnsl = null;
   }
   
   public Word(String d, String t){
      dict = d;
      trnsl = t;
   }
   
   public int compareTo(Word w){
      return trnsl.compareTo(w.trnsl);
   }
   
   public String toString(){
      return dict;
   }
}