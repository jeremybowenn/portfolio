/**
 *A Link List for storing Words
 *
 *@author Jeremy Bowen
 *@version 10/13/17
 */

public class WordList{
   private WordNode head;
   int size;
   
   public WordList(){
      head = new WordNode();
      size = 0;
   }
   
   /**
	 * Displays contents of the list.
	 */
    
	public void display()
	{
      WordNode temp = head;
      while(temp!=null){
         System.out.println(temp.getWord());
         temp=temp.getNext();
      }
   
   
	}
   
   /**
    * Inserts a new Word at the front of the list
    *
	 * @param newWord The new Word to be inserted into the list.
	 */
    
	public void insert(Word newWord)
	{
      WordNode temp = new WordNode();
      temp.changeWord(newWord);
      temp.changeNext(head);
      head = temp;
      size++;
	}


	/**
	 * Search the list for the value val.
	 *
	 * @param val the value to be searched for
    * @param startpos the index to start searching from
	 * @return the index of the found node (-1 if not found)
	 */
    
	public int search(Word val, int startpos)
	{  int i = startpos;
      WordNode temp = head;
      
      if(head==null){
         return -1;
      }      
      while(temp.getNext()!=null){
         if(temp.getWord().compareTo(val)==0){
            return i; 
         }
         temp = temp.getNext();
         i++;
      }
      
      return -1;
	}


	/**
	 * Find first occurrence of the Word (if it exists) and remove it from the list.
	 *
	 * @param word the Word to be removed
	 */
    
	public void remove(Word word)
	{
      WordNode temp;
      temp = head;
      if(head==null){
         
      } else{
         if(head.getWord().compareTo(word)==0)
            head = head.getNext();
         while(temp.getNext() != null){
            if(temp.getNext().getWord().compareTo(word)==0)
               temp.changeNext(temp.getNext().getNext());
            else
               temp = temp.getNext();
         }
      }
   }
   
   /**
    * Gets the size of the Link List
    */
    
   public int getSize(){return size;}
   
   
   /**
    * Gets a Word from a specific index
    *
    * @param i the index to get
    * @return the WordNode at i
    */
   
   public WordNode getWord(int i){
      int j=0;
      WordNode temp = head;
      while((j<=i)&&(temp.getNext()!=null)){
         temp=temp.getNext();
         j++;
      }
      return temp;
   }
}