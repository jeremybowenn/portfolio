/**
 *A Link List for storing Strings
 *
 *@author Jeremy Bowen
 *@version 9/5/17
 */

public class StringList{
   private StringNode head;
   int size;
   
   public StringList(){
      head = new StringNode();
      size = 0;
   }
   
   /**
	 * Displays contents of the list.
	 */
	public void display()
	{
      StringNode temp = head;
      while(temp!=null){
         System.out.println(temp.getString());
         temp=temp.getNext();
      }
   
   
	}
   
   /**
    * Inserts a new String at the front of the list
    *
	 * @param newString The new String to be inserted into the list.
	 */
	public void insert(String newString)
	{
      StringNode temp = new StringNode();
      temp.changeString(newString);
      temp.changeNext(head);
      head = temp;
      size++;
	}


	/**
	 * Search the list for the value val.
	 *
	 * @param val the value to be searched for
    * @param startpos the index to start searching from
	 * @return the index of the found node (-1 if not found)
	 */
	public int search(String val, int startpos)
	{  int i = startpos;
      StringNode temp = head;
      
      if(head==null){
         return -1;
      }      
      while(temp.getNext()!=null){
         if(temp.getString().compareTo(val)==0){
            return i; 
         }
         temp = temp.getNext();
         i++;
      }
      
      return -1;
	}


	/**
	 * Find first occurrence of the String (if it exists) and remove it from the list.
	 *
	 * @param string the string to be removed
	 */
	public void remove(String string)
	{
      StringNode temp;
      temp = head;
      if(head==null){
         
      } else{
         if(head.getString()==string)
            head = head.getNext();
         while(temp.getNext() != null){
            if(temp.getNext().getString()==string)
               temp.changeNext(temp.getNext().getNext());
            else
               temp = temp.getNext();
         }
      }
   }
   
   /**
    * Gets the size of the Link List
    */
   public int getSize(){return size;}
   
   
   /**
    * Gets a String from a specific index
    *
    * @param i the index to get
    * @return the StringNode at i
    */
   
   public StringNode getString(int i){
      int j=0;
      StringNode temp = head;
      while((j<=i)&&(temp.getNext()!=null)){
         temp=temp.getNext();
         j++;
      }
      return temp;
   }
}