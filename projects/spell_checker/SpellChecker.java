import java.util.*;
import java.io.*;

/**
 * A class that checks a given word against a dictionary and replies
 * with phonetic suggestions
 * 
 * @author Jeremy Bowen
 * @version 10/13/17
 */

public class SpellChecker{
   
   DictHash dict;
   TranslateHash trnsl;
   Timer t;
   Scanner kb;
   Metaphone3 m3;
   double loading, parsing, total;
   int dictcount, wordscount, notfoundcount;
   
   /**
    * Constructs the SpellChecker and calls the regular and phonetic dictionary loader
    *
    */
   
   public SpellChecker()throws FileNotFoundException{
      t = new Timer();
      t.start();
      dict = new DictHash();
      
      m3 = new Metaphone3();
      trnsl = new TranslateHash();
      translate();
      t.stop();
      loading = t.seconds();
   }
   
   /**
   * Splits each line of the datafile by white space and prints suggestions and statistics
   *
   */
   
   public void check() throws FileNotFoundException{
      int i;
      int index;
      String [] splitted;
      String temp;
      String phonetic;
      Word word;
      wordscount = 0;
      notfoundcount = 0;
      kb = new Scanner(new FileInputStream("melville.txt"));
      t.start();
      m3.SetEncodeVowels(true);
      while(kb.hasNextLine()){
         temp = kb.nextLine();
         temp =temp.replaceAll("[^a-zA-Z]", " ");
         splitted = temp.split("\\s+");
         for (i=0; i<splitted.length; i++) {
            if (splitted[i].length() > 2 && !splitted[i].matches(".*[A-Z].*")){
               wordscount++;
               if(dict.search(splitted[i], 0)==-1){
                  notfoundcount++;
                  System.out.print("Suggestions for '"+splitted[i]+"': "); 
                  m3.SetWord(splitted[i]);
                  m3.Encode();
                  phonetic = m3.GetMetaph();
                  word = new Word(splitted[i], phonetic);
                  index = trnsl.search(word,0);
                  
                  while((index>-1)&&(index<trnsl.getList(word).getSize()-1)){
                     
                     System.out.print(trnsl.getWord(word, index)+" ");
                     index = trnsl.search(word,index+1);
                  }
                  System.out.println();
               }
                                            
            }
         }
      }
      t.stop();
      parsing = t.seconds();
      System.out.println();
      System.out.println("Words in Dictionary: "+dictcount);
      System.out.println("Words Checked: "+wordscount);
      System.out.println("Words Not Found: "+notfoundcount);
      System.out.println("Time Spent Loading: "+loading);
      System.out.println("Time Spent Parsing: "+parsing);
      System.out.println("Total Time Spent: "+(parsing+loading));
      System.out.println();
      System.out.println("TranslateHash");
      System.out.println("------------------");
      System.out.println("Number of Words: "+trnsl.getSize());
      System.out.println("Capacity: "+trnsl.getTablesize());
      System.out.println("Load Factor: "+(((double) trnsl.getSize()/trnsl.getTablesize())));
      System.out.println("Longest List: "+trnsl.getLongest());
      System.out.println("Number of Lists: "+trnsl.getListCount());
      System.out.println();
      System.out.println("DictHash");
      System.out.println("------------------");
      System.out.println("Number of Words: "+dict.getSize());
      System.out.println("Capacity: "+dict.getTablesize());
      System.out.println("Load Factor: "+((double) dict.getSize()/dict.getTablesize()));
      System.out.println("Longest List: "+dict.getLongest());
      System.out.println("Number of Lists: "+dict.getListCount());
      System.out.println();
      
   }
      
      /**
       * Loads the dictionary and the phonetic dictionary into Hash Tables
       *
       */
      
      public void translate() throws FileNotFoundException{
         kb = new Scanner(new FileInputStream("dictionary.txt"));
         m3.SetEncodeVowels(true);
         String temp, phonetic;
         Word word;
         dictcount = 0;
         while(kb.hasNextLine()){
            temp = kb.nextLine();
            dict.insert(temp);
            dictcount++;
            m3.SetWord(temp);
            m3.Encode();
            phonetic = (m3.GetMetaph());
            word = new Word(temp, phonetic);
            trnsl.insert(word);
         }
         
      }
}