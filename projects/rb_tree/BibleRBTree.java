public class BibleRBTree extends RBTree{
   
   BibleWord[] a = new BibleWord[1000000];
   int i, j, max;
   
   public RBTreeNode<BibleWord> insert(BibleWord data){
      RBTreeNode<BibleWord> prev= findInsertLocation(data);
      if(prev!=NIL&&prev.data.compareTo(data)==0){
         prev.data.increment();
         return prev;
      }
		RBTreeNode<BibleWord> newNode= new RBTreeNode<BibleWord>(data,'R',NIL,prev);
      a[n]=newNode.data;
		insertFixup(newNode,prev);
		n++;
		return newNode;
   }
   
   public RBTreeNode<BibleWord> findInsertLocation(BibleWord data){
      RBTreeNode<BibleWord> prev, x;
		int result;

		prev= NIL;
		x= root;
		while (x != NIL)
		{
			prev= x;
			result= data.compareTo(x.data);
         
         if(result==0){
            return x;
         }
			else if (result < 0)
				x= x.left;
			else
				x= x.right;
		}
		return prev;

   }
   
  public void display(){
      for(int i = n-1; i>n-21; i--){
         System.out.println(a[i]);
      }
   }
   
   public void sort(){
      sort(1, n-1);
   }
   
   public void sort(int p, int r){
      if(p<r){
         int q = partition(p,r);
         sort(p,q);
         sort(q+1,r);         
      }      
    }
    
    private int partition(int p, int r){
      BibleWord k = (BibleWord) a[p];
      int i = p-1;
      int j = r+1;
      do{
         do{j--;} while((a[j].getCount())>(k.getCount()));
         do{i++;} while((a[i].getCount())<(k.getCount()));
         if(i<j){
            swap(i,j);
         }
      }while(i<j);
      return j;
   }
   
   public void swap(int i, int j){
      BibleWord temp = a[i];
      a[i] = a[j];
      a[j] = temp;
   }
      
}