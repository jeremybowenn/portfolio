/**
* Driver for red-black tree implementation of bible words.
*
* @author  T.Sergeant
* @version for Data Structures Homework
*
*/
import java.io.*;
import java.util.*;

public class Driver
{
  public static void main(String[] args) throws FileNotFoundException
  {
  	   BibleRBTree rbtree= new BibleRBTree();
      Scanner s = new Scanner(new FileInputStream("biblewords.txt"));
      double read, process;
      Timer t = new Timer();
      BibleWord bw;
      int i=0;
      t.start();
      while(s.hasNextLine()){
         bw=new BibleWord(s.nextLine());
         rbtree.insert(bw);
      }
      t.stop();
      read=t.seconds();
      t.start();
      rbtree.sort();
      rbtree.display();
      t.stop();
      process=t.seconds();
      
      System.out.println("Reading: "+read);
      System.out.println("Processing: "+process);
      
   

     
  	  /*rbtree.insert(new BibleWord("IN"));
  	  rbtree.insert(new BibleWord("THE"));
  	  rbtree.insert(new BibleWord("BEGINNING"));
  	  rbtree.insert(new BibleWord("GOD"));
  	  rbtree.insert(new BibleWord("CREATE"));
  	  rbtree.insert(new BibleWord("HEAVENS"));
  	  rbtree.insert(new BibleWord("AND"));
  	  rbtree.insert(new BibleWord("EARTH"));
  	  rbtree.insert(new BibleWord("WAS"));
  	  rbtree.insert(new BibleWord("VOID"));
  	  rbtree.insert(new BibleWord("WATERS"));
  	  rbtree.insert(new BibleWord("HOVERED"));
  	  rbtree.insert(new BibleWord("OVER"));
  	  rbtree.insert(new BibleWord("DEEP"));
     rbtree.insert(new BibleWord("MMMMMM"));
     rbtree.insert(new BibleWord("JOKE"));
  	  rbtree.insert(new BibleWord("SHAMGAR"));
  	  rbtree.insert(new BibleWord("AXE"));
  	  rbtree.insert(new BibleWord("MOON"));
  	  rbtree.insert(new BibleWord("POOP"));
  	  rbtree.insert(new BibleWord("ROT"));
     rbtree.insert(new BibleWord("QUERY"));
     rbtree.calcStats();
     System.out.println(rbtree.height());
     System.out.println(rbtree.bheight());
     
  	  rbtree.inorder();*/
  }
}

