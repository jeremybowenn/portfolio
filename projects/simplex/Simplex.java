/**
* A program to solve matrix games using the Simplex method 
*
* Jeremy Bowen
* 
*/

import java.util.Scanner;

public class Simplex{
   static int rows, cols, constraints, unknowns;
   static double[][] table;
   static double[] P;
   static double[] Q;
   
   public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      System.out.print("Number of Constraints: ");
      constraints = s.nextInt();
      System.out.print("Number of Unknowns: ");
      unknowns = s.nextInt();
      rows = constraints+1;
      cols = unknowns+constraints+1;
      table = new double[rows][cols];
      String temp;
      String[] vals;
      String extra = s.nextLine();
      double mostNeg = 0;
      for(int i = 0; i < constraints; i++){
         System.out.println("Row "+(i+1)+", seperated by spaces: ");
         temp = s.nextLine();
         vals = temp.split(" "); 
         
         for(int j = 0; j < unknowns; j++){ 
            table[i][j] = Double.parseDouble(vals[j]);
            if(table[i][j] < mostNeg){
               mostNeg = table[i][j];
            }
         }
      }

      System.out.println();

      for(int i = 0; i < constraints; i++){
         for(int j = 0; j < unknowns; j++){
            table[i][j]-=mostNeg;
         }
      }

      setUpTableau();

      while(check()){ 
         getSimplex();
         
      }
      System.out.println();
      print();  
      getStrategy();
   }

   static void getStrategy(){
      P = new double[constraints];
      Q = new double[unknowns];
      for(int i = 0; i < unknowns; i++){
         for(int j = 0; j < rows; j++){
            if(table[j][i] == 1){
               Q[i] = table[j][cols-1];
               break;
            }
            if(table[i][j] != 1 && table[i][j] != 0){
               Q[i] = 0;
               break;
            }
         }
      }
      
      for(int i = unknowns; i < cols-1; i++){
         P[i-unknowns] = table[rows-1][i];
      }

      System.out.print("P: [");
      for(int i = 0; i < P.length; i++){
         System.out.printf("%.4f  ", P[i]);
      }
      System.out.println("]");

      System.out.print("Q: [ ");
      for(int i = 0; i < Q.length; i++){
         System.out.printf("%.4f  ", Q[i]); 
      }
      System.out.println("]");
      System.out.println();

      System.out.printf("v = %.4f", (1/table[rows-1][cols-1]));
      System.out.println();


   }

   static void setUpTableau(){
      for(int i = 0; i < constraints; i++){
         for(int j = unknowns; j < cols-1; j++){
            if(j == (i+unknowns)){
               table[i][j] = 1;
            }
            else{
               table[i][j] = 0;
            }
         }
         table[i][cols-1] = 1;
      }
      for(int i = 0; i < unknowns; i++){
         table[rows-1][i] = -1;
      }

      for(int i = unknowns; i < cols; i++){
         table[rows-1][i] = 0;
      }


      
   }

   static void print(){
      for(int i = 0; i < rows; i++){
         for (int j = 0; j < cols; j++){
            System.out.printf("%.2f   ", table[i][j]);
         }
         System.out.println();
      }
      System.out.println();
   }

   static void getSimplex(){
      int pivotColumn = findColumn();
      System.out.println("Pivot Column: "+pivotColumn);

      int pivotRow = findRow(pivotColumn);
      System.out.println("Pivot Row: "+pivotRow);

      getNextTable(pivotRow, pivotColumn);
     // print();
   }

   static int findColumn(){
      double[] vals = new double[cols];
      int loc = 0;
      int count = 0;
      int pos = 0;

      for(pos = 0; pos < cols; pos++){
         if(table[rows-1][pos]<0){
            vals[pos] = table[rows-1][pos];
            count++;
         }
         else{
            vals[pos] = 0;
         }
      }
      if(count==0){
         return -1;
      }
      double maxNeg = 0;
      for(int i = 0; i < cols; i++){
         if(vals[i]<maxNeg){
            maxNeg = vals[i];
            loc = i;
         }
      }

     return loc;
   }

   static int findRow(int loc){

      double[] posEntries = new double[rows];
      double[] ratios = new double[rows];
      int negCount = 0;

      for(int i = 0; i < rows; i++){
         if(table[i][loc] > 0){
            posEntries[i] = table[i][loc];
         }
         else{
            posEntries[i] = 0;
            negCount++;
         }
      //System.out.println(posEntries[i]);
      }
      if(negCount == rows){
         return -1;
      }
      else{
         for(int i = 0; i < rows; i++){
            double temp = posEntries[i];
            if(temp>0){
               ratios[i] = table[i][cols-1] / temp;
            }
         }
      }
      
      double min = Double.MAX_VALUE;
      int pos = 0;
      double minindex = 0;
      for(int i = 0; i < ratios.length; i++){
         if(ratios[i] > 0){
            if(min>ratios[i]){
               min = ratios[i];
               pos = i;
            }
         }
      }
      return pos;   

   }

   static void getNextTable(int pivotRow, int pivotColumn){
      double pivot = table[pivotRow][pivotColumn];
      double[] pivotRowVals = new double[cols];
      double[] pivotColumnVals = new double[rows];
      double[] newRow = new double[cols];
     
      System.arraycopy(table[pivotRow], 0, pivotRowVals, 0, cols);

      for(int i = 0; i < rows; i++){
         pivotColumnVals[i] = table[i][pivotColumn]; 
      }
      for(int i = 0; i < cols; i++){
         newRow[i] = pivotRowVals[i]/pivot;
      }

      for(int i = 0; i < rows; i++){
         if(i != pivotRow){
            for(int j = 0; j < cols; j++){
               table[i][j] = table[i][j] - (pivotColumnVals[i]*newRow[j]);
            }
         }
      }
      System.arraycopy(newRow, 0, table[pivotRow], 0, newRow.length);
   }

   static boolean check(){
      int count = 0;

      for(int i = 0; i < cols; i++){
         double val = table[rows-1][i];
         if(val>=0){
            count++;
         }
      }

      if(count == cols){return false;}

      return true;
   }

}
