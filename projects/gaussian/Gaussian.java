/**
 *  A program to perform Gaussian elimination on a system of up to 10 equations and 10 unknowns
 *
 *  Jeremy Bowen
 *  10/14
 */

import java.util.Scanner;
public class Gaussian{
   static double[][] a;
   static int size;

   public static void main(String[] args){
      getSystem();
      int e = forwards();
      
      if(e != -1){
         if(a[e][size]!=0){
            System.out.println("The system is inconsistent.");
            return;
         } else{
            System.out.println("The system has infinite solutions.");
            return;
         }
      }
      
      backwards();
      //print();
   }

   public static int forwards(){
      for(int n = 0; n < size; n++){
         int index = n; 
         double max = a[index][n];
         
         for (int i = n+1; i < size; i++){
            if(Math.abs(a[i][n])>max){
               max = a[i][n];
               index = i;
            }               
         }
         
         if(a[n][index] == 0){
            return n;
         }
         
         if(index != n){
            rowSwap(index, n);
         }
         
         for(int i = n+1; i < size; i++){
            double f = a[i][n] / a[n][n];
            
            for (int j = 0; j <= size; j++){
               a[i][j] -= a[n][j]*f;
            }
            a[i][n] = 0;     
         }
      }
      return -1;
   }
   
   public static void backwards(){
      double[] b = new double[size];  
  
      for (int i = size-1; i >= 0; i--){ 
         b[i] = a[i][size]; 

         for (int j=i+1; j<size; j++){
            b[i] -= a[i][j]*b[j]; 
         }
        
         b[i] = b[i]/a[i][i]; 
      } 
      
      for (int i=0; i<size; i++){
         System.out.print("Unknown "+(i+1)+" is "+b[i]);
         System.out.println();
      } 
   }

   public static void getSystem(){
      Scanner s = new Scanner(System.in);
      System.out.println("How many unknowns? ");
      size = s.nextInt();
            
      a = new double [size][size+1];
      int i = 0;
      int j = 0;
      String temp;
      String extra = s.nextLine();
      String[] temp_array;
      while(i<size){
         System.out.println("Enter row "+(i+1)+" of the system with entries seperated by spaces: ");
         temp = s.nextLine();
         temp_array = temp.split(" ");
         j = 0;
         while(j<size+1){
            a[i][j] = Double.parseDouble(temp_array[j]);
            j++;
         }
         i++;
      }
   }

   public static void rowSwap(int i, int j){
      int n = 0;
      double temp;
      while(n < size+1){
         temp = a[i][n];
         a[i][n] = a[j][n];
         a[j][n] = temp;
         n++;
      }
   }

   public static void print(){
      int i = 0;
      int j;
      while(i<size){
         j=0;
         while(j<size+1){
            System.out.print(a[i][j]+"  ");
            j++;
         }
         System.out.println();
         i++;
      }   
   }
}
 
