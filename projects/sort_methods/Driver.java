import java.io.FileNotFoundException;
public class Driver
{
	public static void main(String [] args) throws FileNotFoundException {
		//switch sorter with file name w/o .java
		Sorter s;
		if(args[0].equals("BuiltInSorter")){ s = new BuiltInSorter(); }
		else if(args[0].equals("MergeSorter")){ s = new MergeSorter(); }
		else if(args[0].equals("SelectionSorter")){ s = new SelectionSorter(); }
		else{System.out.println("Please give a valid sorter name");return;}
      		s.sort();
      		s.display();
	}
}
