/**
 * Sorts the array using a SelectionSort
 *
 * @author Jeremy Bowen
 * @version 12/6/17
 */
 
import java.io.FileNotFoundException;

public class SelectionSorter extends Sorter {
   
   public SelectionSorter() throws FileNotFoundException{
      super();
      sortType = "Selection Sort";
   }
   
   public void sort(){
      t.start();
      for(int i = 0; i<size;i++){
         for(int j = i+1; j<size; j++){
            if(c[i].compareTo(c[j])<0){
               Contributor temp = c[i];
               c[i] = c[j];
               c[j] = temp;
            }
         }
      }
      t.stop();
      sortTime = t.seconds();
   }
}