/**
 * Sorts the array using a mergeSort
 *
 * @author Jeremy Bowen
 * @version 12/6/17
 */
 
import java.io.FileNotFoundException;

public class MergeSorter extends Sorter {
   
   public MergeSorter() throws FileNotFoundException{
      super();
      sortType = "Merge Sort";
   }
   
   /**
    * A  helper for the mergeSort
    *
    */
    
   public void sort(){
      t.start();
      mergeSort(c, 0, size-1);
      t.stop();
      sortTime = t.seconds();

   }
   
   /**
    * Takes an array of Contributors and mergeSorts it
    *
    * @param c the array of Contributors
    * @param l the right index of the sort
    * @param r the left index of the sort
    */
    
   public void mergeSort(Contributor[] c, int l, int r){
      if(l<r){
         int m = l+(r-l)/2;
         mergeSort(c, l, m);
         mergeSort(c, m+1, r);
         combine(c, l, m, r);
      }
   }
   
   /**
    * Combines the two sorted halves
    *
    * @param arr the array to be combined
    * @param l the left index
    * @param r the right index
    * @param m the middle of the indexes
    */
    
   public void combine(Contributor[] arr, int l,int m, int r){
      int i, j, k;
      int n1 = m - l + 1;
      int n2 =  r - m;
 
      Contributor[] L = new Contributor[n1];
      Contributor[] R = new Contributor[n2];      
 
      /**
       * Loads two halves into temp arrays
       */
       
      for (i = 0; i < n1; i++)
         L[i] = arr[l + i];
      for (j = 0; j < n2; j++)
         R[j] = arr[m + 1+ j];
 
      i = 0; 
      j = 0; 
      k = l; 
      
      /**
       * Compares nodes in each array and 
       * copies the appropriate number
       */
       
      while (i < n1 && j < n2){
         if (L[i].compareTo(R[j])>=0){
            arr[k] = L[i];
            i++;
         }else{
            arr[k] = R[j];
            j++;
         }
         k++;
     }
     
     /**
      *  Copies any extra data in each array
      */
     while (i < n1){
        arr[k] = L[i];
        i++;
        k++;
     }
 
     while (j < n2){
        arr[k] = R[j];
        j++;
        k++;
     }
  }
}