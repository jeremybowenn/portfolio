/**
 * A Sorter object containing all the shared data for the different Sorters
 *
 * @author Jeremy Bowen
 * @version 12/6/17
 */
 
import java.util.Scanner;
import java.io.*;

public abstract class Sorter{
   Contributor[] c = new Contributor[2000001];
   Timer t = new Timer();
   int size;
   String sortType;
   double sortTime, loadTime;
   
   /**
    * Constructor that loads data from the given datafile
    *
    * @param datafile The location of the datafile
   */
     
   public Sorter() throws FileNotFoundException{
      t.start();
      Scanner s = new Scanner(new FileInputStream("data.txt"));
      size = 0;
      while(s.hasNextLine()){
         c[size] = new Contributor(s.next(), s.next(), s.nextInt());
         size++;
         s.nextLine();
      }
      t.stop();
      loadTime = t.seconds();

   }
   
   /**
    * Displays the top 10 contributors in the array
    *
    */
    
   public void display(){
      for(int i = 0; i<10; i++){
         System.out.println(c[i]);
      }
      System.out.println();
      System.out.println("Type of sort: "+sortType);
      System.out.println("Number of elements: "+size);
      System.out.println("Time for loading: "+loadTime);
      System.out.println("Time for sorting: "+sortTime);
   }
   
   abstract void sort();
   
   
}