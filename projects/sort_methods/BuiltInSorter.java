/**
 * Sorts the array using the built in Array sort
 *
 * @author Jeremy Bowen
 * @version 12/6/17
 */
 
import java.io.FileNotFoundException;
import java.util.*;

public class BuiltInSorter extends Sorter{
   
   public BuiltInSorter() throws FileNotFoundException{
      super();
      sortType = "Built-in Sort";
   }
   
   public void sort(){
      Contributor[] a = new Contributor[size];
      for(int i = 0; i<size; i++){
         a[i] = c[i];
      }
      t.start();
      Arrays.sort(a, Collections.reverseOrder());
      t.stop();
      sortTime = t.seconds();
      c = a;
      
   }
}