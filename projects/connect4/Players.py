import random


class RandomPlayer:
    def __init__(self, depth_limit=6, name=None):
        self.name = name
        # value will be used to determine first or second player and is set by the game.
        self.value = -1
        self.depth_limit = depth_limit

    def __str__(self):
        return self.name

    def move(self, board, time_left):
        a = [i for i in range(1, 8)]
        random.shuffle(a)
        i = 0
        while i < len(a) and not board.is_legal_move(a[i]):
            i += 1
        if i == len(a):
            return a[0]
        return a[i]

    def set_value(self, value):
        self.value = value

    def get_value(self):
        return self.value


class Human(RandomPlayer):
    def move(self, board, time_left):
        print("You have " + str(time_left()) + " milliseconds remaining")
        return int(input("Choose Column: "))


class Right(RandomPlayer):
    def move(self, board, time_left):
        a = [i for i in range(1, 8)]
        i = 0
        while i < len(a) and not board.is_legal_move(a[i]):
            i += 1
        if i == len(a):
            return a[0]
        return a[i]


class Left(RandomPlayer):
    def move(self, board, time_left):
        a = [i for i in range(8, 1, -1)]
        random.shuffle(a)
        i = 0
        while i < len(a) and not board.is_legal_move(a[i]):
            i += 1
        if i == len(a):
            return a[0]
        return a[i]
