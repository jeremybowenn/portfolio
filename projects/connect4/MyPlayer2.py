from Players import RandomPlayer


class MyPlayer2(RandomPlayer):
    opponent = None
    eval_table = None

    def move(self, board, time_left):
        """
        Move should select the column your agent wants to take on a turn
        :param board: The current state of the board
        :param time_left: The function that calculates the amount of time remain to make a selection
        :return: An int 1-7 for which column to select
        """
        # TODO: Write this method to select a column to make a move
        global opponent
        opponent = board.get_opponent(self)
        self.__create_eval_table()
        i = 1
        while time_left() > 425 and i < 30:
            # print(board.get_opponent(self))
            # move = self.__minimax(board, i)
            move = self.__alphabeta(board, i)
            # print(i)
            i += 1

        return move[1]

    def __eval_fn(self, board):
        """
        The evaluation function that calculates the state of the board
        :param board: The current state of the board
        :return: A value for the state of the board
        """
        # TODO: Write this method to give a value for the board state and return the value
        return self.__compare_to_table(board)

    def __utility(self, board):
        """
        Returns the utility of a board.
        :param board: The current state of the board
        :return:
        """
        # TODO: Write this function to check the utility of the board and return the utility value
        t = board.get_winner()
        if t is None:
            return self.__eval_fn(board)
        else:
            if self.value == t:
                return float("inf")
            else:
                return float("-inf")

    def __minimax(self, board, depth=float("inf"), maximizing_player=True):
        """
        The minimax algorithm to improve the AI's ability
        :param board: The current state of the board
        :param depth: A value for using iterative deepening
        :param maximizing_player: A boolean to know if we are doing the max or min
        :return: returns an int for which column to select for a turn
        """
        # TODO: This will help raise your winning percentage
        # print(depth)
        if depth == 0:
            return self.__utility(board), None

        legal_moves = []
        for c in range(1, board.get_cols()+1):
            if board.is_legal_move(c):
                legal_moves.append(c)

        if len(legal_moves) == 0:
            return self.__utility(board), None

        if maximizing_player is True:
            v = float("-inf")
            choice = legal_moves[0]
            for move in legal_moves:
                temp = self.__minimax(board.forecast_move(self, move), depth - 1, False)[0]
                # print(temp)
                if temp > v:
                    v = temp
                    choice = move
            return v, choice

        else:
            v = float("inf")
            choice = legal_moves[0]
            for move in legal_moves:
                temp = self.__minimax(board.forecast_move(opponent, move), depth - 1, True)[0]
                # print(temp)
                if temp < v:
                    v = temp
                    choice = move

            return v, choice

    # Use if so desired
    def __alphabeta(self, board, depth=float("inf"), maximizing_player=True, alpha=float("-inf"), beta=float("inf")):
        if depth == 0:
            return self.__utility(board), None

        legal_moves = []
        for c in range(1, board.get_cols()+1):
            if board.is_legal_move(c):
                legal_moves.append(c)

        if len(legal_moves) == 0:
            return self.__utility(board), None

        if maximizing_player is True:
            v = float("-inf")
            choice = legal_moves[0]
            for move in legal_moves:
                temp = self.__alphabeta(board.forecast_move(self, move), depth-1, False, alpha, beta)[0]
                # print(temp)
                if temp > v:
                    v = temp
                    choice = move
                if v >= beta:
                    return v, choice
                alpha = max(alpha, v)

            return v, choice

        else:
            v = float("inf")
            choice = legal_moves[0]
            for move in legal_moves:
                temp = self.__alphabeta(board.forecast_move(opponent, move), depth - 1, True, alpha, beta)[0]
                # print(temp)
                if temp < v:
                    v = temp
                    choice = move
                if v <= alpha:
                    return v, choice
                beta = min(beta, v)

            return v, choice

    def __calc_score(self, board):
        score = 3 * 7 + 4 * 6 + 12 + 12
        b = board.get_playable_board()
        for i in range(len(b)):
            for j in range(len(b[i])):
                if b[i][j] == 1:
                    if i == 0 or i == 5:
                        score -= 1
                    if i == 1 or i == 4:
                        score -= 2
                    if i == 2 or i == 3:
                        score -= 3

                    if j == 0 or j == 6:
                        score -= 1
                    if j == 1 or j == 5:
                        score -= 2
                    if j == 2 or j == 4:
                        score -= 3
                    if j == 3:
                        score -= 4

                    if i + j == 3 or i + j == 8:
                        score -= 1
                    if i + j == 4:
                        if i == 0 or i == 4:
                            score -= 1
                        else:
                            score -= 2
                    if i + j == 7:
                        if i == 5 or i == 1:
                            score -= 1
                        else:
                            score -= 2
                    if i + j == 5 or i + j == 6:
                        if i == 2 or i == 3:
                            score -= 3
                        if i == 1 or i == 4:
                            score -= 2
                        if i == 0 or i == 5:
                            score -= 1
        # print(score)
        return score

    def __check_rows(self, board):
        b = board.get_playable_board()
        score = 0
        for i in range(3, 5, 2):
            for j in range(3, 5, 2):
                if b[i][j] != 0:
                    score += self.__check_three_in_a_row(board, i, j)
                    # score += self.__check_two_in_a_row(board, i, j)

        return score

    def __check_three_values(self, board, rc):
        b = board.get_playable_board()
        try:
            if b[rc[0]][rc[1]] == self.value and b[rc[2]][rc[3]] == self.value and b[rc[4]][rc[5]] == self.value:
                return 100
            if b[rc[0]][rc[1]] != self.value and b[rc[0]][rc[1]] != 0 and b[rc[2]][rc[3]] != self.value and \
                    b[rc[2]][rc[3]] != 0 and b[rc[4]][rc[5]] != self.value and b[rc[4]][rc[5]] != 0:
                return -100
        except IndexError:
            pass
        return None

    def __check_two_values(self, board, rc):
        b = board.get_playable_board()
        try:
            if b[rc[0]][rc[1]] == self.value and b[rc[2]][rc[3]] == self.value:
                return 25
            if b[rc[0]][rc[1]] != self.value and b[rc[0]][rc[1]] != 0 and b[rc[2]][rc[3]] != self.value \
                    and b[rc[2]][rc[3]] != 0:
                return -25
        except IndexError:
            pass
        return None

    def __check_three_in_a_row(self, board, row, col):
        # Check going up and down
        t = self.__check_three_values(board, [row, col, row + 1, col, row + 2, col])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row + 1, col, row - 1, col])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row - 1, col, row - 2, col])
        # check going left to right
        t = self.__check_three_values(board, [row, col, row, col - 1, row, col - 2])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row, col - 1, row, col + 1])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row, col + 1, row, col + 2])
        if t is not None:
            return t
        # check going diagonal up
        t = self.__check_three_values(board, [row, col, row + 2, col - 2, row + 1, col - 1])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row + 1, col - 1, row - 1, col + 1])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row - 1, col + 1, row - 2, col + 2])
        if t is not None:
            return t
        # check going diagonal down
        t = self.__check_three_values(board, [row, col, row - 2, col - 2, row - 1, col - 1])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row - 1, col - 1, row + 1, col + 1])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row + 1, col + 1, row + 2, col + 2])
        if t is not None:
            return t
        return 0

    def __check_two_in_a_row(self, board, row, col):
        # Check going up and down
        t = self.__check_two_values(board, [row, col, row + 1, col])
        if t is not None:
            return t
        t = self.__check_two_values(board, [row, col, row - 1, col])
        if t is not None:
            return t
        # check going left to right
        t = self.__check_two_values(board, [row, col, row, col - 1])
        if t is not None:
            return t
        t = self.__check_two_values(board, [row, col, row, col + 1])
        if t is not None:
            return t
        # check going diagonal up
        t = self.__check_two_values(board, [row, col, row + 1, col - 1])
        if t is not None:
            return t
        t = self.__check_two_values(board, [row, col, row - 1, col + 1])
        if t is not None:
            return t
        # check going diagonal down
        t = self.__check_two_values(board, [row, col, row - 1, col - 1])
        if t is not None:
            return t
        t = self.__check_two_values(board, [row, col, row + 1, col + 1])
        if t is not None:
            return t
        return 0

    def __create_eval_table(self):
        global eval_table
        eval_table = [[3, 4, 5, 7, 5, 4, 3],
                      [4, 6, 8, 10, 8, 6, 4],
                      [5, 8, 11, 13, 11, 8, 5],
                      [5, 8, 11, 13, 11, 8, 5],
                      [4, 6, 8, 10, 8, 6, 4],
                      [3, 4, 5, 7, 5, 4, 3]]

    def __compare_to_table(self, board):
        utility = 0
        b = board.get_playable_board()
        for i in range(1, board.get_rows()):
            for j in range(1, board.get_cols()):
                if b[i][j] == self.value:
                    utility += eval_table[i][j]
                if b[i][j] == opponent.value:
                    utility -= eval_table[i][j]
        return utility


