import numpy as np
from copy import deepcopy
from utils import debug


class GameBoard:
    def __init__(self, players):
        self.__players = players
        self.__rows = 6
        self.__cols = 7
        self.__board = np.zeros((self.__rows + 2, self.__cols + 2), dtype=np.int64)

    def __str__(self):
        edge = '-----------------------------\n'
        spacer = '|   |   |   |   |   |   |   |\n'
        result = edge
        for row in range(self.__rows):
            result += spacer
            for col in range(self.__cols):
                result += '| '+str(self.__board[row+1][col+1])+' '
            result = result + '|\n' + spacer + edge
        return result

    def get_row_for_turn(self, col):
        for r in range(self.__rows, 0, -1):
            if self.__board[r][col] == 0:
                return r
        return None

    def get_opponent(self, player):
        for p in self.__players:
            if p != player:
                return p

    def take_turn(self, player, col):
        r = self.get_row_for_turn(col)
        if r is not None:
            self.__board[r][col] = player.get_value()
            return r, col
        return None

    def is_legal_move(self, col):
        if col < 1 or col > self.__cols:
            return False
        for r in range(1, 7):
            if self.__board[r][col] == 0:
                return True
        return False

    def get_playable_board(self):
        return self.__board[1:-1].T[1:-1].T

    def get_board(self):
        return self.__board

    def get_rows(self):
        return self.__rows

    def get_cols(self):
        return self.__cols

    def forecast_move(self, player, col):
        b = deepcopy(self)
        b.take_turn(player, col)
        return b

    def is_game_over(self):
        """
        Returns a tuple of if the game is over, and if it is then a
        list of the 4 in a row or None if the game is not over
        :return: (True/False, results/None)
        """
        c = 1
        board_filled = True
        while c <= self.__cols:
            r = 1
            while r <= self.__rows and self.__board[r][c] == 0:
                r += 1
            if r != 1:
                board_filled = False
            if r <= self.__rows:
                res = self.check_directions(r, c)
                if res is not None:
                    return True, res
            c += 1
        if board_filled:
            return True, None
        return False, None

    def get_winner(self):
        _ , results = self.is_game_over()
        if results is not None:
            return self.__board[results[0]]
        return None

    def __check_values(self, rc):
        try:
            if self.__board[rc[6]][rc[7]] == self.__board[rc[0]][rc[1]] == self.__board[rc[2]][rc[3]] == \
                    self.__board[rc[4]][rc[5]]:
                return True
        except IndexError:
            pass
        return False

    def check_directions(self, row, col):
        # Check going up and down
        if self.__check_values([row, col, row + 1, col, row + 2, col, row + 3, col]):
            return [(row, col), (row + 1, col), (row + 2, col), (row + 3, col)]
        # check going left to right
        if self.__check_values([row, col, row, col - 3, row, col - 2, row, col - 1]):
            return [(row, col), (row, col - 3), (row, col - 2), (row, col - 1)]
        if self.__check_values([row, col, row, col - 2, row, col - 1, row, col + 1]):
            return [(row, col), (row, col - 2), (row, col - 1), (row, col + 1)]
        if self.__check_values([row, col, row, col - 1, row, col + 1, row, col + 2]):
            return [(row, col), (row, col - 1), (row, col + 1), (row, col + 2)]
        if self.__check_values([row, col, row, col + 1, row, col + 2, row, col + 3]):
            return [(row, col), (row, col + 1), (row, col + 2), (row, col + 3)]
        # check going diagonal up
        if self.__check_values([row, col, row + 3, col - 3, row + 2, col - 2, row + 1, col - 1]):
            return [(row, col), (row + 3, col - 3), (row + 2, col - 2), (row + 1, col - 1)]
        if self.__check_values([row, col, row + 2, col - 2, row + 1, col - 1, row - 1, col + 1]):
            return [(row, col), (row + 2, col - 2), (row + 1, col - 1), (row - 1, col + 1)]
        if self.__check_values([row, col, row + 1, col - 1, row - 1, col + 1, row - 2, col + 2]):
            return [(row, col), (row + 1, col - 1), (row - 1, col + 1), (row - 2, col + 2)]
        if self.__check_values([row, col, row - 1, col + 1, row - 2, col + 2, row - 3, col + 3]):
            return [(row, col), (row - 1, col + 1), (row - 2, col + 2), (row - 3, col + 3)]
        # check going diagonal dowm
        if self.__check_values([row, col, row - 3, col - 3, row - 2, col - 2, row - 1, col - 1]):
            return [(row, col), (row - 3, col - 3), (row - 2, col - 2), (row - 1, col - 1)]
        if self.__check_values([row, col, row - 2, col - 2, row - 1, col - 1, row + 1, col + 1]):
            return [(row, col), (row - 2, col - 2), (row - 1, col - 1), (row + 1, col + 1)]
        if self.__check_values([row, col, row - 1, col - 1, row + 1, col + 1, row + 2, col + 2]):
            return [(row, col), (row - 1, col - 1), (row + 1, col + 1), (row + 2, col + 2)]
        if self.__check_values([row, col, row + 1, col + 1, row + 2, col + 2, row + 3, col + 3]):
            return [(row, col), (row + 1, col + 1), (row + 2, col + 2), (row + 3, col + 3)]
        return None

