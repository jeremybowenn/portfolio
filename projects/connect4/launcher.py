from Players import RandomPlayer, Human, Right, Left
from Game import Game
from MyPlayer import MyPlayer
from MyPlayer2 import MyPlayer2

"""
Create the two players for the game. Here you will need to change RandomPlayer to MyPlayer to have your AI agent
play the game.  I would suggest testing your agent as P1 for so many games and as P2. 
"""
p1 = MyPlayer(name="Red")
p2 = MyPlayer2(name="Yellow")

g = Game(p1, p2)
games = 23
r = 0
y = 0
i = 0
while i < games:
    g = Game(p1, p2)
    winner = g.play()
    # print(winner.value)
    if winner is not None:
        if winner.value == 1:
            r += 1
            print(winner)
        if winner.value == 2:
            y += 1
            print(winner)
    i += 1
# g.draw_board()
i = 0
while i < games:
    g = Game(p2, p1)
    winner = g.play()
    # print(winner.value)
    if winner is not None:
        if winner.value == 1:
            y += 1
            print(winner)
        if winner.value == 2:
            r += 1
            print(winner)
    i += 1
print("R:"+str(r)+"   Y:"+str(y))
# The line below would be useful if one of the players is Human since it will
# allow infinite time and displaying the board
# winner = g.play(time_limit=0, display_board=True)
# print("The winner is player "+str(winner))
# g.draw_board()
