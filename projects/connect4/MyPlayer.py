from Players import RandomPlayer


class MyPlayer(RandomPlayer):
    opponent = None
    eval_table = None

    def move(self, board, time_left):
        """
        Move should select the column your agent wants to take on a turn
        :param board: The current state of the board
        :param time_left: The function that calculates the amount of time remain to make a selection
        :return: An int 1-7 for which column to select
        """
        # TODO: Write this method to select a column to make a move
        i = 1
        global opponent
        opponent = board.get_opponent(self)  # initializes opponent to keep through forecast_board changes
        self.__create_eval_table()  # initializes global table once to save time

        while time_left() > 430 and i < 10:
            move = self.__alphabeta(board, i)
            i += 1

        return move[1]

    def __eval_fn(self, board):
        """
        The evaluation function that calculates the state of the board
        :param board: The current state of the board
        :return: A value for the state of the board
        """
        # TODO: Write this method to give a value for the board state and return the value
        return self.__compare_to_table(board)

    def __utility(self, board):
        """
        Returns the utility of a board.
        :param board: The current state of the board
        :return:
        """
        # TODO: Write this function to check the utility of the board and return the utility value
        t = board.get_winner()
        if t is None:
            return self.__eval_fn(board)
        else:
            if self.value == t:
                return float("inf")
            else:
                return float("-inf")

    def __minimax(self, board, depth=float("inf"), maximizing_player=True):
        """
        The minimax algorithm to improve the AI's ability
        :param board: The current state of the board
        :param depth: A value for using iterative deepening
        :param maximizing_player: A boolean to know if we are doing the max or min
        :return: returns an int for which column to select for a turn
        """
        # TODO: This will help raise your winning percentage
        if depth == 0:
            return self.__utility(board), None

        legal_moves = []
        for c in range(1, board.get_cols()+1):
            if board.is_legal_move(c):
                legal_moves.append(c)

        if len(legal_moves) == 0:
            return self.__utility(board), None

        if maximizing_player is True:
            v = float("-inf")
            choice = legal_moves[0]
            for move in legal_moves:
                temp = self.__minimax(board.forecast_move(self, move), depth-1, False)[0]
                if temp > v:
                    v = temp
                    choice = move
            return v, choice

        else:
            v = float("inf")
            choice = legal_moves[0]
            for move in legal_moves:
                temp = self.__minimax(board.forecast_move(opponent, move), depth-1, True)[0]
                if temp < v:
                    v = temp
                    choice = move

            return v, choice

    # Use if so desired
    def __alphabeta(self, board, depth=float("inf"), maximizing_player=True, alpha=float("-inf"), beta=float("inf")):
        if depth == 0:
            return self.__utility(board), None

        legal_moves = []
        for c in range(1, board.get_cols()+1):
            if board.is_legal_move(c):
                legal_moves.append(c)

        if len(legal_moves) == 0:
            return self.__utility(board), None

        if maximizing_player is True:
            v = float("-inf")
            choice = legal_moves[0]
            for move in legal_moves:
                temp = self.__alphabeta(board.forecast_move(self, move), depth-1, False, alpha, beta)[0]
                if temp > v:
                    v = temp
                    choice = move
                if v >= beta:
                    return v, choice
                alpha = max(alpha, v)

            return v, choice

        else:
            v = float("inf")
            choice = legal_moves[0]
            for move in legal_moves:
                temp = self.__alphabeta(board.forecast_move(opponent, move), depth-1, True, alpha, beta)[0]
                if temp < v:
                    v = temp
                    choice = move
                if v <= alpha:
                    return v, choice
                beta = min(beta, v)

            return v, choice

    def __check_rows(self, board):
        """
            Checks the board to see if there are any rows of three or two
            Update: Reduced from entire board to limited rows and cols to try to speed it up
        """
        b = board.get_playable_board()
        score = 0
        for i in range(3, 5, 2):
            for j in range(3, 5, 2):
                if b[i][j] != 0:
                    score += self.__check_three_in_a_row(board, i, j)
                    # score += self.__check_two_in_a_row(board, i, j)

        return score

    def __check_three_values(self, board, rc):
        """
           Given three points checks to see if they are all the opponent's, or our's
           :param board: current board
           :param rc: the values to check
        """
        b = board.get_playable_board()
        try:
            if b[rc[0]][rc[1]] == self.value and b[rc[2]][rc[3]] == self.value and b[rc[4]][rc[5]] == self.value:
                return 100
            if b[rc[0]][rc[1]] != self.value and b[rc[0]][rc[1]] != 0 and b[rc[2]][rc[3]] != self.value and \
                    b[rc[2]][rc[3]] != 0 and b[rc[4]][rc[5]] != self.value and b[rc[4]][rc[5]] != 0:
                return -100
        except IndexError:
            pass
        return None

    def __check_two_values(self, board, rc):
        """
            Same as check_three_values but for two
        """
        b = board.get_playable_board()
        try:
            if b[rc[0]][rc[1]] == self.value and b[rc[2]][rc[3]] == self.value:
                return 25
            if b[rc[0]][rc[1]] != self.value and b[rc[0]][rc[1]] != 0 and b[rc[2]][rc[3]] != self.value\
                    and b[rc[2]][rc[3]] != 0:
                return -25
        except IndexError:
            pass
        return None

    def __check_three_in_a_row(self, board, row, col):
        """
            Given a point, checks all directions for rows
            :param board: the current board
            :param row: the row of the point to check
            :param col: the column of the point to check
        """
        # Check going up and down
        t = self.__check_three_values(board, [row, col, row + 1, col, row + 2, col])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row + 1, col, row - 1, col])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row - 1, col, row - 2, col])
        # check going left to right
        t = self.__check_three_values(board, [row, col, row, col - 1, row, col - 2])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row, col - 1, row, col + 1])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row, col + 1, row, col + 2])
        if t is not None:
            return t
        # check going diagonal up
        t = self.__check_three_values(board, [row, col, row + 2, col - 2, row + 1, col - 1])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row + 1, col - 1, row - 1, col + 1])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row - 1, col + 1, row - 2, col + 2])
        if t is not None:
            return t
        # check going diagonal down
        t = self.__check_three_values(board, [row, col, row - 2, col - 2, row - 1, col - 1])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row - 1, col - 1, row + 1, col + 1])
        if t is not None:
            return t
        t = self.__check_three_values(board, [row, col, row + 1, col + 1, row + 2, col + 2])
        if t is not None:
            return t
        return 0

    def __check_two_in_a_row(self, board, row, col):
        """
            Same as check_three_in_a_row but for two in a row
        """
        # Check going up and down
        t = self.__check_two_values(board, [row, col, row + 1, col])
        if t is not None:
            return t
        t = self.__check_two_values(board, [row, col, row - 1, col])
        if t is not None:
            return t
        # check going left to right
        t = self.__check_two_values(board, [row, col, row, col - 1])
        if t is not None:
            return t
        t = self.__check_two_values(board, [row, col, row, col + 1])
        if t is not None:
            return t
        # check going diagonal up
        t = self.__check_two_values(board, [row, col, row + 1, col - 1])
        if t is not None:
            return t
        t = self.__check_two_values(board, [row, col, row - 1, col + 1])
        if t is not None:
            return t
        # check going diagonal down
        t = self.__check_two_values(board, [row, col, row - 1, col - 1])
        if t is not None:
            return t
        t = self.__check_two_values(board, [row, col, row + 1, col + 1])
        if t is not None:
            return t
        return 0

    def __create_eval_table(self):
        """
            Initializes a global table using the number of winning rows in each position
        """
        global eval_table
        eval_table = [[3, 4, 5, 7, 5, 4, 3],
                      [4, 6, 8, 10, 8, 6, 4],
                      [5, 8, 11, 13, 11, 8, 5],
                      [5, 8, 11, 13, 11, 8, 5],
                      [4, 6, 8, 10, 8, 6, 4],
                      [3, 4, 5, 7, 5, 4, 3]]

    def __compare_to_table(self, board):
        """
           Compares each value of the table to the given board
        """
        utility = 0
        b = board.get_playable_board()
        for i in range(1, board.get_rows()):
            for j in range(1, board.get_cols()):
                if b[i][j] == self.value:
                    utility += eval_table[i][j]
                if b[i][j] == opponent.value:
                    utility -= eval_table[i][j]
        return utility



