# Set to True if you want debug info.
debug_status = True
# If set to False it prints to console otherwise prints to fout file.
debug_to_file = False
fout = "debug.txt"


def debug(s=None):
    if debug_status:
        if debug_to_file:
            with open(fout, 'a') as f:
                f.write(str(s) + "\n")
        else:
            print(s)
