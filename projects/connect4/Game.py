# The debug util can be set in utils.py so that information is written to a console or a file.
from utils import debug
from GameBoard import GameBoard
import copy
import matplotlib.pyplot as plt
import networkx as nx
from networkx.classes import Graph
from time import time


class Game:
    def __init__(self, player1, player2):
        """
        Constructrs a Game object and requires 2 parameters
        :param player1: A object that has a move function for player 1
        :param player2: A object that has a move function for player 2
        """
        self.__last_move = 0
        self.__turn = 0
        self.__p1 = player1
        self.__p2 = player2
        self.__p1.set_value(1)
        self.__p2.set_value(2)
        self.__gb = GameBoard([self.__p1, self.__p2])
        self.__current_player = self.__p1

    # Set the time_limit to a larger time if playing as a human or to 0 for infinite time
    def play(self, time_limit=500, display_board=False):
        """
        Plays the game that has been constructed with the settings
        :param time_limit: If <=0 then infinite time others time_limit in milliseconds
        :param display_board: A boolean to display the board at the end of each turn
        :return: Returns the winning Player
        """
        # print("Games starts")
        while not self.__gb.is_game_over()[0] and self.__turn < (self.__gb.get_rows() * self.__gb.get_cols()):
            self.__current_player = self.__p1 if self.__turn % 2 == 0 else self.__p2
            board = copy.copy(self.__gb)

            def current_time_millis(): return int(round(time() * 1000))
            move_start = current_time_millis()

            def time_left(): return time_limit - (current_time_millis() - move_start)
            pick = self.__current_player.move(board, time_left)
            if time_left() <= 0 < time_limit:
                print("Ran out of Time")
                return self.__p1 if (self.__turn + 1) % 2 == 0 else self.__p2
            debug(str(self.__current_player) + " selected column " + str(pick))
            self.__last_move = self.__gb.take_turn(self.__current_player, pick)
            if self.__last_move is None:
                debug("Invalid move chosen")
            else:
                self.__turn += 1
            # debug(self.__gb)
            debug(str(self.__current_player) + " chooses "+str(self.__last_move))
            if display_board:
                self.draw_board()
        # print("Game Over")
        winning_set = self.__gb.is_game_over()[1]
        # print("Winning set is "+str(winning_set))
        if winning_set is None:
            return None
        return self.__current_player

    def draw_board(self):
        """
        Draws the board as a graph
        :return: None
        """
        g = Graph()
        top = 5000
        colors = ['grey', 'red', 'yellow']
        node_colors = []

        for row in range(self.__gb.get_rows()):
            for col in range(self.__gb.get_cols()):
                label = '(' + str(row + 1) + ',' + str(col + 1) + ')'
                g.add_node(label, pos=(col*500, top-row*500))
                node_colors.append(colors[self.__gb.get_playable_board()[row][col]])
        node_pos = {n: g.node[n]['pos'] for n in g}
        nx.draw_networkx_nodes(g, node_pos, node_color=node_colors, node_size=500)
        # Uncomment the following 4 lines if you want labels on the dots on the board
        # labels = {}
        # for node in g:
        #     labels[node] = node
        # nx.draw_networkx_labels(g, node_pos, labels)
        plt.axis('off')
        plt.show()

