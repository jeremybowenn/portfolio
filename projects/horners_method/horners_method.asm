;; A program to estimate pi using Horner's method.
;;
;; Jeremy Bowen
;; 11/18/18

%include "../lib/iomacros.asm"
		
		section .data
cutoff:		dq	0.000000001
pi:		dq	3.14159265358979323846
two:		dq	2.0
one:		dq	1.0
zero:		dq	0.0
str:		db	"Estimation for pi: ", 0
endl		db	10, 0

		section .bss
ans:		resq	1
temp:		resq	1

		section .text
		global		main
		extern	fabs
main:
		movsd	xmm1, [zero]	; n=0
top:		
		movsd	xmm0, xmm1
                call	estimate_pi	; estimate_pi(n)
		addsd	xmm1, [one]	; n++
		movsd	[ans], xmm0	; store answers
		movsd	[temp], xmm1
		put_str	str		; "Estimation for pi: "
		put_dbl	[ans]		; print(estimate)
		put_str	endl
		movsd	xmm0, [ans]	; retrieve estimate
		subsd	xmm0, [pi]	; estimate - pi
		call	fabs		; abs(estimate - pi)
		movsd	xmm1, [temp]	; retrieve n
		ucomisd	xmm0, [cutoff]	; cmp(error, cutoff)
		jbe	alldone
		jmp	top

alldone:        mov     ebx,0           ; return 0
                mov     eax,1           ; on
                int     80h             ; exit
	

		; A function for estimating pi using Honer's method given an n
	
estimate_pi:	
		movsd	xmm8, [one]	; estimate
 
loop:		ucomisd	xmm0, [zero]	; cmp(n, 0)
		jbe	return
		movsd	xmm10, xmm0	
		mulsd	xmm8, xmm10	; estimate = estimate*n
		mulsd	xmm10,[two]	; 2*n
		addsd	xmm10, [one]	; 2*n+1
		divsd	xmm8, xmm10	; estimate = n*estimate/2n+1

		addsd	xmm8, [one]	; estimate+1
		subsd	xmm0, [one]	; n--;
		jmp	loop
		
		
return:		movsd	xmm0, xmm8	
		mulsd	xmm0, [two]	; pi/2 * 2
		ret			; an estimate of pi
		
