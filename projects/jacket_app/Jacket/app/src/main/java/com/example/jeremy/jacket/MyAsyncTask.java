package com.example.jeremy.jacket;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * For getting the temperature from the weather.gov website
 *
 * Created by Jeremy on 4/8/2018.
 */

public class MyAsyncTask extends AsyncTask<Void, Void, String> {
    Element body;
    Element div;
    Elements p;
    String url;

    public MyAsyncTask(String url){
        super();
        this.url = url;
    }

    /**
     * Gets the temperature from weather.gov
     * @param params
     * @return Element the temperature from the page
     */
    @Override
    protected String doInBackground(Void... params) {
        String temp = "";
        Document doc;
        try {
            doc = Jsoup.connect(url).get();
            body = doc.body();
            div = body.getElementById("current_conditions-summary");
            p = div.getElementsByClass("myforecast-current-lrg");
            temp = p.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return temp;
    }
}
