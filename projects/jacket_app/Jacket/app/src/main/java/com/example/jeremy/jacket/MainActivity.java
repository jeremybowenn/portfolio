package com.example.jeremy.jacket;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    String temperature;
    int tempint;
    int[] tempoptions;
    String response;
    double longitude;
    double latitude;
    String zipcode;
    String url;

    LocationManager lm;
    final int REQUEST_LOCATION = 1;

    TextView txt;
    Button btnOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        tempoptions = new int[4];
        tempoptions[0] = 65;
        tempoptions[1] = 55;
        tempoptions[2] = 45;
        tempoptions[3] = 35;

        getLocation();

        Log.d("loc", "onCreate: "+latitude+"    "+longitude);
        getURL();
        Log.d("url", "onCreate: "+url);
        getTemp();
        Log.d("temp", "onCreate: "+temperature);

        checkJacket();

        btnOptions = findViewById(R.id.btnOptions);
        btnOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchOptionsActivity();
            }
        });
    }

    public void launchOptionsActivity(){
        Intent intent = new Intent(this, OptionsActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1)
        {
            tempoptions=data.getIntArrayExtra("tempoptions");
            checkJacket();
        }
    }

    /**
     * Checks permissions and gets the latitude and longitude of the device
     */
    void getLocation(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED ){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        else{
            Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if(location!=null){
                Log.d("", "getLocation: in if");
                longitude = location.getLongitude();
                latitude = location.getLatitude();
            } else{
                Log.d("", "getLocation: Unable to find location.");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case REQUEST_LOCATION:
                getLocation();
                break;
        }
    }

    /**
     * Uses a Geocoder object to get an address from the given logitude and latitude. Working.
     */
    public void getURL(){
        try {
            final Geocoder gc = new Geocoder(this);
            Address address = gc.getFromLocation(latitude, longitude, 1).get(0);
            Log.d("", "getURL: "+address);
            zipcode = address.getPostalCode();
            Log.d("", "getURL: "+zipcode);

        } catch(IOException e){
            Log.d("TAG", "getURL: "+e.toString());
            zipcode = "";
        }
        url = "http://weather.gov/"+zipcode;
        Log.d("", "getURL: "+url);
    }

    /**
     * Passes the url to the AsyncTask to get the weather information for the zipcode. Working.
     */
    public void getTemp(){
        try {
            temperature = new MyAsyncTask(url).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            temperature = e.toString();
        }

        txt = findViewById(R.id.txt);

        try {
            tempint = Integer.parseInt(temperature.replaceAll("[\\D]", ""));
        } catch (NumberFormatException e) {
            tempint = -1;
        }
        try {
            temperature = new MyAsyncTask(url).execute().get();
        } catch(InterruptedException | ExecutionException e){
            temperature = e.toString();
        }

    }



    /**
     * If statements to print out to the screen whether one needs a Jacket based on the temperature. Working.
     */
    public void checkJacket() {
        if(tempoptions!=null) {
            if (tempint >= tempoptions[0]) {
                response = "You don't need a jacket";
                txt.setText(response);
            }

            if (tempint >= tempoptions[1] && tempint < tempoptions[0]) {
                response = "You will need a light jacket.";
                txt.setText(response);
            }

            if (tempint >= tempoptions[2] && tempint < tempoptions[1]) {
                response = "You will need a medium jacket.";
                txt.setText(response);
            }

            if (tempint < tempoptions[3]) {
                response = "You will need a heavy jacket.";
                txt.setText(response);
            }
        } else{
            response = "Error";
            txt.setText(response);
        }
    }
}



