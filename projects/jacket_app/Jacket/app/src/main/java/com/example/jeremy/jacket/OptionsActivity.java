package com.example.jeremy.jacket;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class OptionsActivity extends AppCompatActivity {
    int[] tempoptions;

    TextView txtNoJacket;
    TextView txtLight;
    TextView txtMedium;
    TextView txtHeavy;
    EditText edtxtNoLow;
    EditText edtxtLightLow;
    EditText edtxtMediumLow;
    EditText edtxtHeavyHigh;

    Button btnFinish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        tempoptions = new int[4];


        btnFinish = findViewById(R.id.btnFinish);

        txtNoJacket = findViewById(R.id.txtNoJacket);
        txtLight = findViewById(R.id.txtLight);
        txtMedium = findViewById(R.id.txtMedium);
        txtHeavy = findViewById(R.id.txtHeavy);
        edtxtNoLow = findViewById(R.id.edtxtNoLow);
        edtxtLightLow = findViewById(R.id.edtxtLightLow);
        edtxtMediumLow = findViewById(R.id.edtxtMediumLow);
        edtxtHeavyHigh = findViewById(R.id.edtxtHeavyHigh);

        edtxtNoLow.requestFocus();

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edtxtNoLow.getText().toString().equals("")&&!edtxtLightLow.getText().toString().equals("")&&!edtxtMediumLow.getText().toString().equals("")&&!edtxtHeavyHigh.getText().toString().equals("")) {
                    tempoptions[0] = Integer.parseInt(edtxtNoLow.getText().toString());
                    tempoptions[1] = Integer.parseInt(edtxtLightLow.getText().toString());
                    tempoptions[2] = Integer.parseInt(edtxtMediumLow.getText().toString());
                    tempoptions[3] = Integer.parseInt(edtxtHeavyHigh.getText().toString());

                    Intent intent = new Intent();
                    intent.putExtra("tempoptions", tempoptions);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });


    }
}
