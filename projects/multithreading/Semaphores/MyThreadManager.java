/**
 * Keeps track of which threads are busy and which ones can be assigned work.
 *
 */

import java.net.Socket;
import java.io.PrintStream;
public class MyThreadManager
{
	private boolean [] busy;   // which threads are busy
	private Thread [] workers; // array of threads

	/**
	 * We default to 5 threads in server pool.
	 */
	public MyThreadManager() { this(5); }


	/**
	 * Initially none of the threads is busy.
	 *
	 * @param n number of threads allowed in server pool
	 */
	public MyThreadManager(int n)
	{
		workers= new Thread[n];
		busy= new boolean[n];
		for (int i=0; i<n; i++)
			busy[i]= false;
	}


	/**
	 * Find the first non-busy thread and return it.
	 *
	 * @return position in array of a non-busy thread
	 */
	private int nextOpen()
	{
		for (int i=0; i<workers.length; i++)
			if (!busy[i] || workers[i].getState()==Thread.State.TERMINATED)
				return i;
		return -1;
	}


	/**
	 * Create a new server thread to handle incoming request.
	 */
	public void assign(Socket con, BookDatabase db) throws Exception
	{
		int pos;
		PrintStream ps = new PrintStream(con.getOutputStream());
		pos= nextOpen();	// look for an unused slot
		if (pos<0){				// if all busy then
			System.out.println("Denied connection");
			ps.println("The server is busy. Try again later.");	//tells the client the server is busy before closing
			con.close();		// close the connection
		}
		else {
			ps.println("");	//tells the client the server can continue
			busy[pos]= true;
			workers[pos]= new Thread(new ServerThread(con,db));
			workers[pos].start();
			System.out.println("Assigned connection in slot "+pos+" to "+workers[pos].getName());
		}
	}
}
